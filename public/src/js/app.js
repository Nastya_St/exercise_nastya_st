
var button = document.querySelector('#start-button');
var output = document.querySelector('#output');

button.addEventListener('click', function() {
  // Create a new Promise here and use setTimeout inside the function you pass to the constructor
  // Создайте здесь new Promise и используйте setTimeout внутри функции, которую вы передаете конструктору

  var promise1 = new Promise((resolve, reject) => {
    setTimeout(function() { // <- Store this INSIDE the Promise you created! <- Сохраните это ВНУТРИ Promise, которое вы создали!
      // Resolve the following URL: https://swapi.co/api/people/1
      resolve('https://swapi.co/api/people/1');
    }, 3000);
  });

  // Handle the Promise "response" (=> the value you resolved) and return a fetch()
  // call to the value (= URL) you resolved (use a GET request)
  // Обрабатываем "ответ" Promise (=> значение, которое вы разрешили) и возвращаем fetch()
  // обращение к значению (= URL), которое вы разрешили (используйте запрос GET)


  promise1.then( function(url){
    console.log('Promise1::Resolved URL = ', url);
    return fetch(url);
  })

  // Handle the response of the fetch() call and extract the JSON data, return that
  // and handle it in yet another then() block
  // Обрабатываем ответ на вызов fetch() и извлекаем данные JSON, возвращаем их
  // и обработать его в еще одном блоке then()

  .then(function(response){
    return response.json();
  })

  // Finally, output the "name" property of the data you got back (e.g. data.name) inside
  // the "output" element (see variables at top of the file)
  // Наконец, выведите свойство "name" для данных, которые вы получили (например, data.name) внутри
  // элемент "output" (см. переменные вверху файла)

  .then(function(data){
    console.log('Promise1::JSON data = ', data);
    output.textContent = data.name;
  })
  .catch(function(err){
    console.log('Promise1::Error occurred: ', err);
  })

  // Repeat the exercise with a PUT request you send to https://httpbin.org/put
  // Make sure to set the appropriate headers 
  // Send any data of your choice, make sure to access it correctly when outputting it
  // Example: If you send {person: {name: 'Max', age: 28}}, you access data.json.person.name
  // to output the name (assuming your parsed JSON is stored in "data")
  // Повторите упражнение с запросом PUT, который вы отправляете на https://httpbin.org/put
  // Обязательно установите соответствующие заголовки
  // Отправьте любые данные по вашему выбору, убедитесь, что вы правильно обращаетесь к ним при выводе
  // Пример: если вы отправляете {person: {name: 'Max', age: 28}}, вы получаете доступ к data.json.person.name
  // для вывода имени (при условии, что ваш проанализированный JSON хранится в «данных»)

  var promise2 = new Promise((resolve, reject) => {
    setTimeout(function() {
      resolve('https://httpbin.org/put');
    }, 3000);
  });

  promise2.then(function(url){
    console.log('Promise2::Resolved URL = ', url);
    return fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          name: 'Anastasia',
          age: 18
      })
    });
  })
  .then(function(response){
    return response.json();
  })
  .then(function(responseJSON){
    console.log('Promise2::JSON data name = ', responseJSON.json.name);
    console.log('Promise2::JSON data age = ', responseJSON.json.age);
    output.textContent = responseJSON.json.name;
  })

  // To finish the assignment, add an error to URL and add handle the error both as
  // a second argument to then() as well as via the alternative taught in the module
  // Чтобы закончить назначение, добавьте ошибку в URL и добавьте обработку ошибки как
  // второй аргумент для then(), а также через альтернативу, описанную в модуле
  .catch(function(err){
    console.log('Promise2::Error occurred::caught in catch: ', err);
  });
  
});